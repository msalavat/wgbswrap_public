# WGBSwrap
This workflow is designed for analysis of whole genome bisulphite data

First wrapper script: WGBS_QC_Bsseeker.sh
1. FastQC check prior to trimming
3. Trimming using TrimGalore
4. Generation of bisulphite ameanable 3-letter reference genome with BSseeker2
5. Alignment with BSSeeker and Bowtie2
6. Duplicate read removal with picardtools

Second wrapper script: WGBS_Cgmaptools_MethPipe.sh
7. Methylation calling with CGmaptools
8. Coverage statisitcs and visualizations with CGMaptools
9. Symmetric CpG merge and identification of hypo and hyper methylated regions with MethPipe
10. Storing bedgraph file of methylation levels at individual CpG sites

DOI: https://doi.org/10.1101/2020.07.06.189480

Publication title: 

Global analysis of transcription start sites in the new ovine reference genome (Oar rambouillet v1.0)

Mazdak Salavati1*, Alex Caulton2,3*, Richard Clark4, Iveta Gazova1,5, Tim P. Smith6, Kim C. Worley7, Noelle E. Cockett8, Alan L. Archibald1, Shannon Clarke2, Brenda Murdoch9, Emily L. Clark1 and The Ovine FAANG Consortium

1The Roslin Institute and Royal (Dick) School of Veterinary Studies, University of Edinburgh, Edinburgh, UK
2AgResearch, Invermay Agricultural Centre, Mosgiel, New Zealand
3University of Otago, Dunedin, New Zealand
4Genetics Core, Clinical Research Facility, University of Edinburgh, Edinburgh, UK
5Institute for Genetics and Molecular Medicine, University of Edinburgh, Edinburgh, UK
6USDA, ARS, USMARC, Clay Center, Nebraska, USA
7Baylor College of Medicine, Houston, Texas, USA
8Utah State University, Logan, Utah, USA
9University of Idaho, Moscow, Idaho, USA

*These two authors contributed equally to the work
Corresponding author: emily.clark@roslin.ed.ac.uk
