#!/bin/bash
#This is the script for the analysis
#SGE flags
#$ -N FAANG_WGBS_cgmaptools_methpipe        ##TBC1
#$ -cwd
#$ -l h_rt=48:00:00
#$ -l h_vmem=20G
#$ -r yes
#$ -notify
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 8
#$ -V
#$ -t 1-8
#$ -tc 2


#The list of all tools used in the pipeline apart from CGmaptools scripts and MethPipe scipts which were loaded from the $HOME/tools directory
module load python/3.7.3
module load pigz/2.3.4

python --version

RUN_PATH=/dataset/AC_phd/scratch/WGBS/FAANG
cd ${RUN_PATH}

genome=${RUN_PATH}/ref_genomes/Oar_ramb1.0_lambda.fna
cgmaptools=${HOME/tools/CGMaptools/cgmaptools}
methpipe=${HOME/tools/methpipe-3.4.3/bin}
infile=$(ls -1 ${RUN_PATH}/input/*SAM*_R1.fq.gz | xargs -n 1 basename -s _R1.fq.gz | awk "NR==${SLURM_ARRAY_TASK_ID}")


# call methylation levels from the mapping result and produce 3 output files (ATCGmap, CGmap, wig) for each sample
if [ -f "${RUN_PATH}/CGmap_MethPipe/${infile}.ATCGmap.gz" ];then
        echo "bam2cgmap complete"
else
        function run_bam2cgmap(){
                cgmaptools convert bam2cgmap -b ${RUN_PATH}/BS_out/${infile}.bs.rmdup.bam -g ${genome} --rmOverlap -o ${RUN_PATH}/CGmap_MethPipe/${infile}
        }
        run_bam2cgmap
fi

# remove scaffold data from ATCGmap and CGmap files for downstream analysis and extract reads aligned to lamba for QC (requires chromosome text files)
if [ -f "${RUN_PATH}/CGmap_MethPipe/${infile}_NoScaffs.ATCGmap.gz" ];then
        echo "scaffolds already removed, proceeding to graphical output"
else
        function remove_scaffolds(){
                pigz -dc ${RUN_PATH}/CGmap_MethPipe/${infile}.ATCGmap.gz | grep -f ${RUN_PATH}/oar_ramb_chr.txt | pigz > ${RUN_PATH}/CGmap_MethPipe/${infile}_NoScaffs.ATCGmap.gz &
		pigz -dc ${RUN_PATH}/CGmap_MethPipe/${infile}.CGmap.gz | grep -f ${RUN_PATH}/oar_ramb_chr.txt | pigz > ${RUN_PATH}/CGmap_MethPipe/${infile}_NoScaffs.CGmap.gz
		pigz -dc ${RUN_PATH}/CGmap_MethPipe/${infile}.CGmap.gz | grep "NC_001416.1" | pigz > ${RUN_PATH}/CGmap_MethPipe/${infile}_Lambda.CGmap.gz

        }
        remove_scaffolds
fi

# run cgmaptools to generate genome coverage statistics and graphical outputs
if [ -f "${RUN_PATH}/CGmap_MethPipe/${infile}.mec_stat.data" ];then
        echo "coverage stats have already been generated, proceed to methpipe workflow"
else
        function run_coverage_stats(){
		cgmaptools oac bin -i ${RUN_PATH}/CGmap_MethPipe/${infile}_NoScaffs.ATCGmap.gz -B 5000000 -f png -p ${infile} -t ${infile} > ${RUN_PATH}/CGmap_MethPipe/${infile}.oac_bin.data
		cgmaptools mec bin -i ${RUN_PATH}/CGmap_MethPipe/${infile}_NoScaffs.CGmap.gz -B 5000000 -f png -p ${infile} -t ${infile} > ${RUN_PATH}/CGmap_MethPipe/${infile}.mec_bin.data
		cgmaptools oac stat -i ${RUN_PATH}/CGmap_MethPipe/${infile}_NoScaffs.ATCGmap.gz -f png -p ${infile} > ${RUN_PATH}/CGmap_MethPipe/${infile}.oac_stat.data
		cgmaptools mec stat -i ${RUN_PATH}/CGmap_MethPipe/${infile}_NoScaffs.CGmap.gz -f png -p ${infile} > ${RUN_PATH}/CGmap_MethPipe/${infile}.mec_stat.data
	}
	run_coverage_stats
fi

# reformat CGmap files for compatability with MethPipe
if [ -f "${RUN_PATH}/CGmap_MethPipe/${infile}.meth.levels" ];then
        echo "meth files already generated, proceed to methpipe workflow"
else
        function create_meth_file(){
		zcat ${RUN_PATH}/CGmap_MethPipe/${infile}_NoScaffs.CGmap.gz |
		awk '{print $1, $3, $2, $4, $6, $8}' |
		awk '{ if ($3 == "G") {$3 = "-"} else if ($3 == "C") {$3 = "+"} {print}}' |
		sed 's/CG/CpG/' > ${RUN_PATH}/CGmap_MethPipe/${infile}.meth
		grep -v -e -- ${RUN_PATH}/CGmap_MethPipe/${infile}.meth | sed 's/CHG/CXG/' > ${RUN_PATH}/CGmap_MethPipe/${infile}.meth.levels
	}
	create_meth_file
fi

# methpipe workflow
	function run_methpipe_workflow(){
		# carry out futher data manipulation with the MethPipe programme
		# merge symmetric CpG pairs
		methpipe/symmetric-cpgs -o  ${RUN_PATH}/CGmap_MethPipe/${infile}_CpGs.meth  ${RUN_PATH}/CGmap_MethPipe/${infile}.meth
		
		# identify hypomethylated regions
		# the hmr programme  uses a hidden Markov model (HMM) approach using a Beta-Binomial distribution to describe methylation levels at individual sites
		#  while accounting for the number of reads informing those sites
		methpipe/hmr -o  ${RUN_PATH}/CGmap_MethPipe/${infile}_CpGs.hypo.dmr  ${RUN_PATH}/CGmap_MethPipe/${infile}_CpGs.meth
		
		# identify hyper methylated regions - first invert the CpG.meth files then rerun hmr program
		awk '{$5=1-$5; print $0}'  ${RUN_PATH}/CGmap_MethPipe/${infile}_CpGs.meth >  ${RUN_PATH}/CGmap_MethPipe/${infile}_CpGs_inverted.meth
		methpipe/hmr -o  ${RUN_PATH}/CGmap_MethPipe/${infile}.hyper.dmr  ${RUN_PATH}/CGmap_MethPipe/${infile}_CpGs_inverted.meth
		
		# Compute statistics for the output of methcounts
		methpipe/levels -o ${RUN_PATH}/CGmap_MethPipe/${infile}.levels.metrics ${RUN_PATH}/CGmap_MethPipe/${infile}.meth.levels
		
		# convert symmetric CpG file to bedgraph format for visualization in Gvis with read depth cutoff of 10x
		cat ${RUN_PATH}/CGmap_MethPipe/${infile}_CpGs.meth |
		cut -c4- |
		awk -F '\t' '{if($6>10)print}' |
		awk 'BEGIN {FS="\t"}; {print $1" "$2" "$2+1" "$5}' > ${RUN_PATH}/CGmap_MethPipe/${infile}_CpGs_10x.bedgraph

	}
	run_methpipe_workflow

# visulization of CpG methylation can be performed with the R package Gviz
