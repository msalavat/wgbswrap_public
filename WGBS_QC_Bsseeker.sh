#!/bin/bash
#This is the script for the analysis
#SGE flags
#$ -N FAANG_WGBS_WRAP        ##TBC1
#$ -cwd
#$ -l h_rt=288:00:00
#$ -l h_vmem=20G
#$ -r yes
#$ -notify
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 8
#$ -V
#$ -t 1-8
#$ -tc 2

#The list of all tools used in the pipeline part from BSseeker2. The BSseeker2 scripts were loaded from the $HOME/tools directory
# Please refer to manusript for complete list of tools used in the pipeline.

#The list of all tools used in the pipeline part from BSseeker2. The BSseeker2 scripts and CGMapTools were loaded from the $HOME/tools directory
# Please refer to manusript for complete list of tools used in the pipeline. 

module load fastqc/0.11.7
module load cutadapt/1.9.1
module load TrimGalore/0.5.0
module load python/2.7.13
module load bowtie/2.3.4.3
module load samtools/1.6
module load picard/2.17.11

#Make sure of the absolute path for the RUN_PATH variable. 
RUN_PATH= YOURFOLDERSTRUCTURE/FAANG_WGBS 							##TBC2
cd ${RUN_PATH}


if [ -d "${RUN_PATH}/Reports" ];then
        echo "output directories already exist, all set"
else
        function make_output_directories(){
                mkdir -p ${RUN_PATH}/Reports
		mkdir -p ${RUN_PATH}/Trim_out
		mkdir -p ${RUN_PATH}/BS_out
		mkdir -p ${RUN_PATH}/BS_out/Oar_ramb1.0_lambda
		mkdir -p ${RUN_PATH}/CGmap_MethPipe
                mkdir -p ${RUN_PATH}/tmp

        }
        make_output_directories
fi

REPORTS=${RUN_PATH}/Reports 
TEMP=${RUN_PATH}/tmp
VCPU=32
BSbuild=${HOME}/tools/BSseeker2/bs_seeker2-build.py
BSalign=${HOME}/tools/BSseeker2/bs_seeker2-align.py
genome=${RUN_PATH}/ref_genomes/Oar_ramb1.0_lambda.fna

#The input fastq files were placed in the ${RUN_PATH/input} folder for all the sampels e.g. n= 8 in this run. 
#This number of samples is used as the task array number (-pe sharedmem 8) so each task job would process 1 pair of fastq files.

infile=$(ls -1 ${RUN_PATH}/input/*SAM*_R1.fq.gz | xargs -n 1 basename -s _R1.fq.gz | awk "NR==${SLURM_ARRAY_TASK_ID}")

# run fastqc on raw (merged) data

if [ -f "${REPORTS}/${infile}_R1_fastqc.html" ];then
        echo "fastqc has already been carried out on raw data, proceed to trimming"
else
        function run_fastqc_rawdata(){
                fastqc -t ${VCPU} ${RUN_PATH}/input/${infile}_R1.fq.gz -o $REPORTS &
                fastqc -t ${VCPU} ${RUN_PATH}/input/${infile}_R2.fq.gz -o $REPORTS
               

        }
        run_fastqc_rawdata
fi

# trim raw reads 
# Accel-NGS Methyl Seq library preparation introduces a low complexity tail (8N) to reads
# 18 bp are trimmed off the 5’-end of R2 and the 3’ end of R1 to remove bases derived from the sequence tag introduced in the library preparation procedure

if [ -f "${RUN_PATH}/Trim_out/${infile}_R1_val_1.fq.gz" ];then
        echo "trimming has already been carried out on raw data, proceed to mapping"
else
        function run_trimming(){
                trim_galore -q 20 --fastqc --fastqc_args "--outdir ${REPORTS}" --paired --clip_R2 18 --three_prime_clip_R1 18 --retain_unpaired \
-o ${RUN_PATH}/Trim_out ${RUN_PATH}/input/${infile}_R1.fq.gz ${RUN_PATH}/input/${infile}_R2.fq.gz


        }
        run_trimming
fi

# check for presence of bisulphite ameneable reference sheep genome and build 3-letter converted genome if required
# NOTE the Enterobacteria phage lambda complete genome was added as a chromosome to the Oar_Ramb_1.0 genome to map control spike-in reads
# the purpose of the unmethylated lambda phage control spike-in is to assess the efficiency of bisulphite conversion during library preparation
# this was done with the command $ cat Oar_ramb1.0.fna Enterobacteria_phage_lambda.fna > Oar_ramb1.0_lambda.fna

if [ -f "${RUN_PATH}/BS_out/Oar_ramb1.0_lambda/Oar_ramb1.0_lambda.fna_bowtie2/C_C2T.1.bt2" ];then
	echo "BSRef genome exists"
else
	function build_BSRef(){
   		python $BSbuild -f ${genome} --aligner=bowtie2 -d ${RUN_PATH}/BS_out/Oar_ramb1.0_lambda
	}
	build_BSRef
fi

# map reads on 3-letter converted genome
if [ -f "${RUN_PATH}/BS_out/${infile}.bs.bam" ];then
        echo "mapping has already been carried out on data, proceed to duplicate identification"
else
        function run_mapping(){
		python ${BSalign} -1 ${RUN_PATH}/Trim_out/${infile}_R1_val_1.fq.gz -2 ${RUN_PATH}/Trim_out/${infile}_R2_val_2.fq.gz \
-g ${genome} -m 4 --aligner=bowtie2 -d ${RUN_PATH}/BS_out/Oar_ramb1.0_lambda/ -l 4000000 --bt2-p ${VCPU} \
-o ${RUN_PATH}/BS_out/${infile}.bs.bam --temp_dir=${TEMP} -M ${RUN_PATH}/BS_out/${infile}.multhits -u ${RUN_PATH}/BS_out/${infile}.unmapped

	}
	run_mapping
fi


# sort bam files and remove duplicate reads

if [ -f "${RUN_PATH}/BS_out/${infile}.bs.rmdup.bam" ];then
        echo "bam files have already been sorted and duplicate reads removed, further analysis can be performed with script: WGBS_Cgmaptools_MethPipe.sh"
else
        function run_bamTidy(){
	samtools sort -@ ${VCPU} -m 3G -T ${TEMP} -o ${RUN_PATH}/BS_out/${infile}.bs.sorted.bam ${RUN_PATH}/BS_out/${infile}.bs.bam
picard MarkDuplicates \
INPUT=${RUN_PATH}/BS_out/${infile}.bs.sorted.bam \
OUTPUT=${RUN_PATH}/BS_out/${infile}.bs.rmdup.bam \
METRICS_FILE=${REPORTS}/${infile}.rmdup.metrics \
ASSUME_SORTED=TRUE REMOVE_DUPLICATES=TRUE

	}
	run_bamTidy
fi

# Analysis continues in script WGBS_Cgmaptools_MethPipe.sh
